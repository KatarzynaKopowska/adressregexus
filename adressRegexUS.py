import os, re, pyperclip

#US adress regex

adressRegex = re.compile(r'''
((\d){,5})
\s
(.*\s?.*?)
\s
([A-Z]{2})
\s
((\d){5})''', re.VERBOSE | re.IGNORECASE)

#copy part of text in which you want to find adress [US]

text = str(pyperclip.paste())
matches = []
for groups in adressRegex.findall(text):
    adress = ''.join([groups[1], groups[2], groups[3], groups[4]])
    matches.append(adress)

if len(matches) > 0:
    print('Your file has been created in following location: ' + os.getcwd())
    adressFile = open('adress.txt', 'w')
    adressFile.write('\n'.join(matches))
    adressFile.close()

else:
    print('There were no matches.')